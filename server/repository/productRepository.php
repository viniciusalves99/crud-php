<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/testePhp/server/dao/productDao.php';

class ProductRepository
{
    private $productDao;

    function __construct()
    {
        $this->productDao = new ProductDao();
    }

    public function getProducts()
    {
        $products = $this->productDao->selectAllProducts();
        return $products;
    }

    public function createProduct($data)
    {
        $products = $this->productDao->createProduct($data);
        return $products;
    }

    public function updateProduct($data)
    {
        $products = $this->productDao->updateProduct($data);
        return $products;
    }

    public function deleteProduct($id)
    {
        $products = $this->productDao->deleteProduct($id);
        return $products;
    }
}
