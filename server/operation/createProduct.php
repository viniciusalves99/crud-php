<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/testePhp/server/repository/productRepository.php';
$productController = new ProductRepository();

if (isset($_POST["name"]) && isset($_POST["value"]) && isset($_POST["image"]) && isset($_POST["description"])) {
    if (
        empty($_POST["name"]) || empty($_POST["value"]) || empty($_POST["description"])
    ) {
    } else {
        $data = array("price" => $_POST["value"], "nome" => $_POST["name"], "image" => $_POST["image"], "description" => $_POST["description"]);
        $productController->createProduct($data);
    }
}

header("Location: /testePhp/index.php");
die();
