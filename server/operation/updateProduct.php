<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/testePhp/server/repository/productRepository.php';
$productRepository = new ProductRepository();

if (isset($_POST["id"]) && isset($_POST["name"]) && isset($_POST["value"]) && isset($_POST["image"]) && isset($_POST["description"])) {
    if (empty($_POST["id"]) || empty($_POST["name"]) || empty($_POST["value"]) || empty($_POST["description"])) {
    } else {
        $data = array("id" => $_POST["id"], "price" => $_POST["value"], "nome" => $_POST["name"], "image" => $_POST["image"], "description" => $_POST["description"]);
        $productRepository->updateProduct($data);
    }
}

die();
