<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/testePhp/config.php';

class ProductDao
{

    private $db;

    function __construct()
    {
        $this->db = pg_connect(Config::POSTGRES_CONN_STRING);
    }

    public function selectAllProducts()
    {
        $result = pg_query($this->db, "
        select
            id,
            price,
            nome,
            image,
            description
        from public.produto p
        order by id;
        ");
        $row = pg_fetch_all($result);
        return $row;
    }

    public function updateProduct($data)
    {
        $result = pg_query($this->db, "
        UPDATE public.produto
        SET price={$data['price']}, nome='{$data['nome']}', image='{$data['image']}', description='{$data['description']}'
        WHERE id={$data['id']};
        ");
        $row = pg_fetch_all($result);
        return $row;
    }

    public function createProduct($data)
    {
        $result = pg_query($this->db, "
        INSERT INTO public.produto
        (price, nome, image, description)
        VALUES({$data['price']}, '{$data['nome']}', '{$data['image']}', '{$data['description']}')
        ");
        $row = pg_fetch_all($result);
        return $row;
    }

    public function deleteProduct($id)
    {
        $result = pg_query($this->db, "
        DELETE FROM public.produto
        WHERE id=$id;
        ");
        $row = pg_fetch_all($result);
        return $row;
    }
}
