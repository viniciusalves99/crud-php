<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/testePhp/server/repository/productRepository.php';
$productRepository = new ProductRepository();
$productRepository->deleteProduct(4);
$products = $productRepository->getProducts();

?>

<div class="row justify-content-center m-5">
    <form class="card col-md-6 p-5" action='server/operation/createProduct.php' method="POST">
        <div class="form-row">
            <div class="form-group col-md-8">
                <label for="name">Nome:</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Nome do produto">
            </div>
            <div class="form-group col-md-4">
                <label for="value">Valor:</label>
                <input type="number" class="form-control" id="value" name="value" placeholder="Valor do produto">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="image">Imagem:</label>
                <input type="text" class="form-control" id="image" name="image" placeholder="Endereço da imagem">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md">
                <label for="description">Descrição:</label>
                <input type="text" class="form-control" id="description" name="description" placeholder="Descrição do produto">
            </div>
        </div>
        <div class="form-row justify-content-end">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </form>
</div>

<div class="justify-content-center m-5">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">Codigo</th>
                <th scope="col">Nome</th>
                <th scope="col">Preço</th>
                <th scope="col">Imagem</th>
                <th scope="col">Descrição</th>
                <th scope="col" class="acoesHeader">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($products as $product) {
                echo "
                <tr id='product{$product['id']}'>
                    <th scope='row'>{$product['id']}</th>
                    <td>{$product['nome']}</td>
                    <td>{$product['price']}</td>
                    <td><a href='#' onclick=openImage('{$product['image']}')>Abrir imagem</a></td>
                    <td>{$product['description']}</td>
                    <th class='acoes'>
                        <button type='button' onclick=loadModal({$product['id']}) class='btn btn-outline-primary'>Atualizar</button>
                        <button type='button' onclick=deleteProduct({$product['id']}) class='btn btn-outline-danger'>Excluir</button>
                    </th>
                </tr>
                ";
            }
            ?>
        </tbody>
    </table>
</div>

<div id="modalUpdate" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Atualizar produto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-8">
                                <label for="name">Nome:</label>
                                <input id="name-modal" type="text" class="form-control" name="name" placeholder="Nome do produto">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="value">Valor:</label>
                                <input id="value-modal" type="number" class="form-control" name="value" placeholder="Valor do produto">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="image">Imagem:</label>
                                <input id="image-modal" type="text" class="form-control" name="image" placeholder="Endereço da imagem">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md">
                                <label for="description">Descrição:</label>
                                <input id="description-modal" type="text" class="form-control" name="description" placeholder="Descrição do produto">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick=updateProduct() class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalImage" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Atualizar produto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img id='image-show' src="" alt="Produto" width="400" height="500">
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/0f60281b2d.js"></script>
<script>
    var selectedId = '';

    function findProduct(id) {
        var products = <?= json_encode($products) ?>;

        var product = products.filter(p => p.id == id);

        return product[0];
    }

    async function deleteProduct(id) {
        $.post("server/operation/deleteProduct.php", {
            id: id
        }, function(data) {
            $('#product' + id).hide();
            console.log(data);
        })
    }

    function loadModal(id) {
        selectedId = id;
        var product = findProduct(id);
        $('#name-modal').val(product.nome);
        $('#value-modal').val(product.price);
        $('#description-modal').val(product.description);
        $('#image-modal').val(product.image);

        console.log(product);
        $('#modalUpdate').modal('show');
    }

    function updateProduct() {
        var id = selectedId;
        var name = $('#name-modal').val();
        var value = $('#value-modal').val();
        var description = $('#description-modal').val();
        var image = $('#image-modal').val();

        var requestData = {
            id: id,
            name: name,
            value: value,
            description: description,
            image: image
        }

        $.post("server/operation/updateProduct.php", requestData, function(data) {
            location.reload()
        })
    }

    function openImage(url) {
        console.log(url);
        $('#image-show').attr('src', url);
        $('#modalImage').modal('show');
    }
</script>